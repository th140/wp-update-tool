#!/bin/bash

if [[ ! -f ~/.local/bin/wp ]]; then
    mkdir -p ~/.local/bin
    curl -o ~/.local/bin/wp https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
    chmod +x ~/.local/bin/wp
else
    echo "File ~/.local/bin/wp already exists. To upgrade it, delete ~/.local/bin/wp and re-run this script"
fi

if  ! which wp > /dev/null 2>&1 ; then
    echo "The wp-cli command was installed at $HOME/.local/bin, but can't be found. Please ensure that $HOME/.local/bin is included in PATH in .bash_profile" > /dev/stderr
    exit 127
fi
