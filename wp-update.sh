#!/bin/bash

if [[ ! -f wp-config.php ]]; then
    echo "This doesn't seem to be a Wordpress directory"
    exit 127
fi

wp core version
wp core update
if wp core is-installed --network; then
    wp core update-db --network
else
    wp core update-db
fi
wp plugin update --all
wp theme update --all
