#!/bin/bash


if which php > /dev/null 2>&1; then
    php --version
    echo "If PHP version shown above is >= 5.6, you should be able to use wp-cli." > /dev/stderr
else
    if [[ -d /opt/rh ]]; then
        echo "PHP is not in the path" > /dev/stderr
        echo "Update ~/.bash_profile and add the preferred version of PHP to your path, i.e. one of:" > /dev/stderr
        find /opt/rh -maxdepth 1 -name 'rh-php*'
    else
        echo "No PHP found and no /opt/rh directory" > /dev/stderr
        exit 127
    fi
    echo "See the README for help setting up the PHP path." > /dev/stderr
fi
