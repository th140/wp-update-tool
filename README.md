# Scripts overview

* install-wp.sh - installs wp-cli in a standard user location
* php-check.sh - verifies that PHP is configured and that wp-cli will run on the configured version of PHP
* update-wp.sh - updates Wordpress
* update-wp-network.sh - use this instead of `update-wp.sh` for wordpress multisite/network installations.

# One-time setup

## Clone this repository to your home directory

```bash
$ git clone https://gitlab.oit.duke.edu/th140/wp-update-tool.git ~/wp-update-tool
```

## Set up `.bash_profile`

Before proceeding, run `php-check.sh`. If it reports the PHP version of >= 5.6, you're all set. Otherwise, continue with this section.

Open `.bash_profile` and ensure that the `PATH` variable includes the `/home/YOURUSERNAME/.local/bin` path and a path to a PHP executable in `/opt/rh/rh-php{v}/root/bin`, where `{v}` is the version of PHP installed, e.g. 56, 74, etc. To see what versions are available, run `find /opt/rh -maxdepth 1 -name 'rh-php*'`.

Usually a properly configured `PATH` variable in `~/.bash_profile` will look something like this:

```
PATH=/opt/rh/rh-php56/root/bin:/home/YOURUSERNAME/.local/bin:$PATH
```

The `.bash_profile` file is read automatically when logging in, but it needs to be sourced anytime changes are made.  To source it, run `. ~/.bash_profile`

To verify that the desired version of PHP is being found, run `php-check.sh`

## Install wp-cli

Run `install-wp-cli.sh`. This places wp in `~/.local/bin`. If you get a message about it being unavailable / not found, you'll need to edit `~/.bash_profile` to ensure that it is in the PATH (see above).

After this is installed, you'll be able to run `wp` from anywhere on the system.

## Update Wordpress

* Navigate to the docroot, e.g. `/srv/web/mysite.duke.edu/html`
* Run `~/wp-update-tool/update-wp.sh`. This will update core, plus all plugins, and themes.
